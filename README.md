# Node Express template project

## Préparation
1. Assurez-vous d'avoir les permissions `Owner` sur le groupe [`sopra-steria-uness/forks`](https://gitlab.com/sopra-steria-uness/forks).
1. Créer un fork de ce projet `express-app` via le bouton **Fork**. Créez le fork dans le groupe `sopra-steria-uness/forks`, et donnez-lui votre nom afin d'éviter les conflits de nom.

## Partie 1 - Un premier pipeline
Ajouter un fichier `.gitlab-ci.yml` à la racine du projet et ajoutez 2 jobs :
- Un premier job qui exécute eslint avec la commande `npm run lint`
- Un second job **dans un second stage** qui exécute les tests unitaires de l'application avec `npm run test`

Observer les logs des 2 jobs et trouver ce qui pourrait être amélioré.

## Partie 2 - Cache
1. Faire en sorte que les jobs n'aient besoin de télécharger les dépendances que si le fichier `package-lock.json` a été modifié (les dépendances sont stockées dans le répertoire `node_modules`)
1. Observer le gain de temps d'exécution des jobs

## Partie 3 - Artifacts
1. Modifier le job de test pour qu'il exporte son rapport de tests `tests-results.xml` dans GitLab
1. Modifier le job de lint pour appeler `npm run lint-report`. Le rapport de lint sera alors écrit dans un fichier `lint-report.json`
1. Faire en sorte que le rapport de lint soit exporté en tant qu'artifact vers un 3° job **dans un 3° stage** qui affichera ce rapport avec [jq](https://jqlang.github.io/jq/). **Note :** l'image `ghcr.io/jqlang/jq:1.7.1` est l'image OCI officielle de jq
1. Faire en sorte que le rapport de lint **ne soit pas téléchargé** par le job de test

## Partie 4 - Variables & valeurs par défaut
1. Ajouter un nouveau job **dans un 4° stage** exécutant `npm publish` en vous appuyant sur [la documentation GitLab](https://docs.gitlab.com/ee/user/packages/npm_registry/#publishing-a-package-by-using-a-cicd-pipeline)
1. Factoriser le code de votre pipeline en profitant du mot-clé `default`

## Partie 5 - Contrôle de l'exécution du pipeline
1. Faire en sorte que le job `npm publish` ne s'exécute que sur les pipelines relatifs à un tag
1. Faire en sorte que le job `npm publish` attende uniquement la fin des jobs de lint et de test pour démarrer, sans attendre l'exécution du job `jq`

## Partie 6 - Inclusions & héritage
Modfier le pipeline pour que tous les jobs héritent d'un job commun décrit dans un fichier `yml` à part, qui décrit les propriétés génériques de tous les jobs `npm`.

## Partie 7 - Releases
Ajouter un nouveau job déclenchant la création d'un tag et d'une release
- Le job doit s'exécuter uniquement sur la branche principale du projet
- Le numéro de version de la release est à récupérer dans l'attribut `version` du fichier package.json
